# WordPress Assessment
Het doel van dit Assessment is om de basiskennis van WordPress te testen.

Je hebt voor deze opdracht geen plugins nodig, probeer alles zoveel mogelijk op de manier
te doen die WordPress aanraadt.

## De opdracht
De opdracht bestaat uit de volgende onderdelen:
- Download/installeer de laatste versie van WordPress. Deze kun je via een lokale server draaien. Mocht
je geen lokale server geïnstalleerd hebben dan vind je onderaan dit document een korte uitleg over een tweetal
   verschillende lokale webservers.
   
### Thema
1. Verwijder de bestaande thema's en creëer je eigen, nieuwe, thema.
2. Stel de permalinks in op de instelling die jou het best lijkt.
3. Maak de onderstaande post types aan:
    1. Diensten
    2. Producten
4. Maak een taxonomie aan genaamd 'Soorten' en ken deze toe aan beide post types
5. Maak voor 1 van beide post types de volgende templates en toon hier de betreffende content:
    1. Archief - toon hier maximaal 3 posts die je via het dashboard gevuld hebt (titel, samenvatting en uitgelichte 
       afbeelding)
    2. Single - toon hier in ieder geval de titel, de content en de uitgelichte afbeelding
6. Maak een menu aan en toon deze op de pagina. Het menu moet een submenu hebben. Je hoeft verder geen 'on hover' 
   events in te bouwen voor het submenu, maar het mag gewoon altijd zichtbaar zijn naast/onder het hoofdmenu.
7. Toon de berichten op je bij stap 6 gemaakte archief op willekeurige volgorde.
8. Als de gebruiker ingelogd is toon dan op elke pagina hoe vaak deze persoon al ingelogd is geweest. Indien er geen 
   gebruiker ingelogd is toon je niets.
   Hiervoor moet je dus:
   1. Per gebruiker een counter bijhouden als deze gebruiker een login actie uitvoert.
   2. Deze counter tonen aan de voorkant

### Plugin
1. Maak een simpele WordPress plugin die elke gebruiker
   die niet ingelogd is doorstuurt/redirect naar de login pagina. Een gebruiker die niet ingelogd is kan dan dus geen 
   enkele pagina bekijken en wordt altijd geredirect naar de loginpagina.

## Styling
Styling is niet de hoofdzaak van deze Assessment, al is het wel belangrijk om een duidelijke layout/grid te gebruiken
bij de verschillende onderdelen. Bijv. bij stap 6 moet je wel onderscheid hebben tussen de verschillende posts op 
het archief.

Het is mogelijk om een CSS framework te gebruiken, bijvoorbeeld `Bootstrap`.

### Lokale ontwikkelomgeving
Bij Nordique gebruiken wij Valet (https://laravel.com/docs/8.x/valet) voor onze lokale ontwikkel omgevingen. Let op: 
als je Valet gebruikt zul je los MySQL moeten installeren.
Je kan ook gebruik maken van bijvoorbeeld MAMP (https://www.mamp.info/en/downloads/), in dat geval heb je ook direct 
de mogelijkheid om een database te hosten.

## Deliverables
Lever ons in ieder geval de volgende zaken aan:
- De codebase (de volledige WordPress map inclusief je nieuwe thema)
- De database
- Inloggegevens voor het WordPress dashboard
- Korte beschrijving van wat je gedaan hebt en eventueel uitleg voor bepaalde keuzes. Als je wijzigingen
buiten het thema doet geef dan aan in welk bestand en waarom.


